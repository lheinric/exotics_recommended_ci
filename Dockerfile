FROM atlas/analysisbase
ADD . /code
WORKDIR /code
RUN sudo chown -R atlas /code && \
    source /home/atlas/release_setup.sh && \
    echo "your build instructions here" && \
    echo "time stamp: $(date)" > /code/build.stamp && \
    git rev-parse --short HEAD > /code/build.revision
